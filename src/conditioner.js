// Import function

import { postRollHook, preRollHook } from "./scripts/CustomItemRollHandler.js";

Hooks.once("init", function() {
    console.log("conditioner | Intializing");
});

Hooks.once("setup", function() {
    console.log("conditioner | Setup");
});

Hooks.once("ready", function() {
});

Hooks.on("preRollItemBetterRolls", preRollHook);

Hooks.on("rollItemBetterRolls", postRollHook);

Hooks.on(`renderChatMessage`, (message, html, data) => {
	updateSaveButtons(html);
});

export function updateSaveButtons(html) {
	html.find(".card-buttons.bi-card-button > button").click(event => {
		const button = event.currentTarget;
		if (button.dataset.action === "bi") {
			event.preventDefault();
            let r = new Roll("1d6");
            r.toMessage();
		}
	});
}