export function preRollHook(roll) {

    const actorToken = getActorTokenById(roll.actor.data._id);
    const actorConditions = game.cub.getConditions(actorToken)

    if(!actorConditions) {
        return
    }

    roll.updateForPreset();

    actorConditions.map(it => it.name).forEach(it => {
        if(it === "bless" && roll.fields && roll.fields[0][0] === "attack") {
            roll.fields[0].push({bonus: "1d4"})
        }
        if(it === "bane" && roll.fields && roll.fields[0][0] === "attack") {
            roll.fields[0].push({bonus: "- 1d4"})
        }
    });

    roll.params.preset = null
}

export function postRollHook(roll) {
    const actorToken = getActorTokenById(roll.actor.data._id);
    const actorConditions = game.cub.getConditions(actorToken)

    if(!actorConditions) {
        return
    }

    actorConditions.map(it => it.name).forEach(it => {
        if(it === "bardic inspiration") {
            roll.templates.push({type:"Bardic Ins", html: '<div class="card-buttons red-card-buttons bi-card-button"><button data-action="bi"> Bardic Inspiration (1d6)</button></div>'})
        }
    });
}

function getActorTokenById(actorId) {
    let actor = canvas.tokens.placeables.find(t => t.actor?._id === actorId);
    return actor;
}