export function setupDamageHandler() {
    // Patch the Core Roll 
    const og_rollDamage = game.dnd5e.entities.Item5e.prototype.rollDamage;
    game.dnd5e.entities.Item5e.prototype.rollDamage = function rollDamage({event, spellLevel=null, versatile=false}={}) {
        rollDamageWrapper({event, spellLevel, versatile}, this, og_rollDamage);
    };
}


function rollDamageWrapper({event, spellLevel=null, versatile=false}={}, base_item, og_rollDamage) {
    const itemData = base_item.data.data;
    const actor = base_item.actor;
    const targetTokens = [...game.user.targets];

    console.log("conditioner | ", actor)
    if (targetTokens.length === 0) {
        return og_rollDamage.call(base_item, {event, spellLevel, versatile});
    }

    const targetToken = targetTokens[0];
    const conditions = game.cub.getConditions(targetToken)

    if(!conditions) {
        return og_rollDamage.call(base_item, {event, spellLevel, versatile});
    }

    let parts = itemData.damage.parts;
    const parts_og = [...itemData.damage.parts];

    conditions.map(it => it.name).forEach(it => {
        if(it === "Hex" && actorHasHex(actor)) {
            parts.push(["1d6", "necrotic"])
        }
    });

    let promise = og_rollDamage.call(base_item, {event, spellLevel, versatile});

    itemData.damage.parts = parts_og;

    return promise
}


function actorHasHex(actor) {
    let yea = false
    actor.items.forEach((value, key, map) => {
       if(value.data.name === "Hex") yea = true;
    });

    return yea;
}
